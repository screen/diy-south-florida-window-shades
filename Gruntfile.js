module.exports = function (grunt) {

    // Carga todas las dependecias de grunt
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-autoprefixer');

    // Configuracion de grunt
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // Configuracion del plugin grunt-contrib-copy
        copy: {
            main: {
                expand: true,
                cwd: 'src/css',
                src: ["**/*.css"],
                dest: 'dist/css'
            }
        },

        // Configuracion del plugin grunt-contrib-cssmin
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'css',
                    src: ['one-last-thing.css'],
                    dest: 'css',
                    ext: '.min.css'
                }]
            }
        },

        // Configuracion del plugin grunt-contrib-less
        less: {
            dev: {
                options: {
                    paths: ['src/less']
                },
                files: [{
                    expand: true,
                    cwd: 'css',
                    src: ['one-last-thing.less'],
                    ext: '.css',
                    dest: 'css'
                }]
            },
        },

        // Configuracion del plugin grunt-autoprefixer
        autoprefixer: {
            dev: {
                files: [{
                    expand: true,
                    cwd: 'css',
                    src: ['one-last-thing.css'],
                    dest: 'css',
                    ext: '.css'
                }]
            },
        },

        // Configuracion del plugin grunt-contrib-watch
        watch: {
            options: {
                nospawn: true,
                livereload: true
            },
            less: {
                files: ['css/one-last-thing.less'],
                tasks: ['less:dev', 'autoprefixer:dev']
            }
        }

    });

    // Tarea por defecto
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('dev', ['less:dev', 'autoprefixer:dev']);
    grunt.registerTask('prod', ['less:dev', 'autoprefixer:dev', 'cssmin']);
};