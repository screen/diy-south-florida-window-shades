jQuery(document).ready(function(e) {
    var dataActual = [];
    var dataActual2 = [];
    var dataCol = jQuery.parseJSON(custom_admin_url.custom_window_width);
    var dataRow = jQuery.parseJSON(custom_admin_url.custom_window_height);

    /* TABLE 2 */
    var dataActual = custom_admin_url.custom_table_price_1;
    dataActual = dataActual.replace(/\\/g, "");

    if (dataActual != '') {
        jQuery("#specialPrice1").handsontable({
            data: JSON.parse(dataActual),
            rowHeaders: dataRow,
            colHeaders: dataCol,
            contextMenu: false,
            licenseKey: 'non-commercial-and-evaluation'
        });
    } else {
        getTable(dataRow, dataCol).then((data) => {
                dataActual = data;
                jQuery("#specialPrice1").handsontable({
                    data: dataActual,
                    rowHeaders: dataRow,
                    colHeaders: dataCol,
                    contextMenu: false,
                    licenseKey: 'non-commercial-and-evaluation'
                });
            })
            .catch((error) => {
                console.log(error)
            });
    }
    /* TABLE 2 */
    var dataActual2 = custom_admin_url.custom_table_price_2;
    dataActual2 = dataActual2.replace(/\\/g, "");

    if (dataActual2 != '') {
        jQuery("#specialPrice2").handsontable({
            data: JSON.parse(dataActual2),
            rowHeaders: dataRow,
            colHeaders: dataCol,
            contextMenu: false,
            licenseKey: 'non-commercial-and-evaluation'
        });
    } else {
        getTable(dataRow, dataCol).then((data) => {
                dataActual2 = data;
                jQuery("#specialPrice2").handsontable({
                    data: dataActual2,
                    rowHeaders: dataRow,
                    colHeaders: dataCol,
                    contextMenu: false,
                    licenseKey: 'non-commercial-and-evaluation'
                });
            })
            .catch((error) => {
                console.log(error)
            });
    }

    var dataActual3 = custom_admin_url.custom_table_price_3;
    dataActual3 = dataActual3.replace(/\\/g, "");

    if (dataActual3 != '') {
        jQuery("#specialPrice3").handsontable({
            data: JSON.parse(dataActual3),
            rowHeaders: ['3"', '4"'],
            colHeaders: dataCol,
            contextMenu: false,
            licenseKey: 'non-commercial-and-evaluation'
        });
    } else {
        getTableFascia(2, dataCol).then((data) => {
                console.table(data);
                dataActual3 = data;
                jQuery("#specialPrice3").handsontable({
                    data: dataActual3,
                    rowHeaders: ['3"', '4"'],
                    colHeaders: dataCol,
                    contextMenu: false,
                    licenseKey: 'non-commercial-and-evaluation'
                });
            })
            .catch((error) => {
                console.log(error)
            });
    }

    jQuery('#saveTable1').on('click', function(e) {
        e.preventDefault();
        var hotInstance = jQuery("#specialPrice1").handsontable('getInstance');
        jQuery.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                action: 'save_table_data',
                table: 1,
                info: JSON.stringify(hotInstance.getData())
            },
            beforeSend: function() {
                jQuery('#loader1').removeClass('loader-hidden');
            },
            success: function(response) {
                jQuery('#loader1').addClass('loader-hidden');
                jQuery('#responseTable1').html('Cambios Guardados');
                setTimeout(function() {
                    jQuery('#responseTable1').html('');
                }, 2000);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                reject(error);
            }
        });
    });

    jQuery('#saveTable2').on('click', function(e) {
        e.preventDefault();
        var hotInstance = jQuery("#specialPrice2").handsontable('getInstance');
        jQuery.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                action: 'save_table_data',
                table: 2,
                info: JSON.stringify(hotInstance.getData())
            },
            beforeSend: function() {
                jQuery('#loader2').removeClass('loader-hidden');
            },
            success: function(response) {
                jQuery('#loader2').addClass('loader-hidden');
                jQuery('#responseTable2').html('Cambios Guardados');
                setTimeout(function() {
                    jQuery('#responseTable2').html('');
                }, 2000);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                reject(error);
            }
        });
    });

    jQuery('#saveTable3').on('click', function(e) {
        e.preventDefault();
        var hotInstance = jQuery("#specialPrice3").handsontable('getInstance');
        jQuery.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                action: 'save_table_data',
                table: 3,
                info: JSON.stringify(hotInstance.getData())
            },
            beforeSend: function() {
                jQuery('#loader2').removeClass('loader-hidden');
            },
            success: function(response) {
                jQuery('#loader2').addClass('loader-hidden');
                jQuery('#responseTable2').html('Cambios Guardados');
                setTimeout(function() {
                    jQuery('#responseTable2').html('');
                }, 2000);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                reject(error);
            }
        });
    });

    jQuery('#saveTable4').on('click', function(e) {
        e.preventDefault();
        jQuery.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                action: 'save_sideguide_data',
                info: jQuery('#sideguideInput').val()
            },
            beforeSend: function() {
                jQuery('#loader4').removeClass('loader-hidden');
            },
            success: function(response) {
                jQuery('#loader4').addClass('loader-hidden');
                jQuery('#responseTable4').html('Cambios Guardados');
                setTimeout(function() {
                    jQuery('#responseTable4').html('');
                }, 2000);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                reject(error);
            }
        });
    });
});

function getWidth() {
    var tmp = [];
    jQuery.ajax({
        type: 'POST',
        url: ajaxurl,
        data: {
            action: 'get_attributes_prices_width'
        },
        success: function(response) {
            var respuesta = jQuery.parseJSON(response);
            for (let i = 0; i < respuesta.length; i++) {
                tmp[i] = respuesta[i];
            }
            return tmp;
        },
        error: function(jqXHR, textStatus, errorThrown) {
            reject(error);
        }
    });
}

function getHeight() {
    var tmp = [];
    jQuery.ajax({
        type: 'POST',
        url: ajaxurl,
        data: {
            action: 'get_attributes_prices_height'
        },
        success: function(response) {
            var respuesta = jQuery.parseJSON(response);
            for (let i = 0; i < respuesta.length; i++) {
                tmp[i] = respuesta[i];
            }
            return tmp;
        },
        error: function(jqXHR, textStatus, errorThrown) {
            reject(error);
        }
    });
}

function getTable(dataRow, dataCol) {
    return new Promise((resolve, reject) => {
        var tmp = [];
        for (var i = 0; i < dataRow.length; i++) {
            var dataTrans = [];
            for (var y = 0; y < dataCol.length; y++) {
                dataTrans[y] = null;
            }
            tmp[i] = dataTrans;
        }
        resolve(tmp);
    });
}

function getTableFascia(dataRow, dataCol) {
    return new Promise((resolve, reject) => {
        var tmp = [];
        for (var i = 0; i < 2; i++) {
            var dataTrans = [];
            for (var y = 0; y < dataCol.length; y++) {
                dataTrans[y] = null;
            }
            tmp[i] = dataTrans;
        }
        resolve(tmp);
    });
}