var priceHTML = '';
var selectWidth = '';
var selectHeight = '';
var priceHidden = '';

/* CUSTOM ON LOAD FUNCTIONS */
function documentCustomLoad() {
    "use strict";
    console.log('Functions Correctly Loaded');

    AOS.init({
        // Global settings:
        disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
        startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
        initClassName: 'aos-init', // class applied after initialization
        animatedClassName: 'aos-animate', // class applied on animation
        useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
        disableMutationObserver: false, // disables automatic mutations' detections (advanced)
        debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
        throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)
        offset: 120, // offset (in px) from the original trigger point
        delay: 0, // values from 0 to 3000, with step 50ms
        duration: 400, // values from 0 to 3000, with step 50ms
        easing: 'ease', // default easing for AOS animations
        once: false, // whether animation should happen only once - while scrolling down
        mirror: false, // whether elements should animate out while scrolling past them
        anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation
    });

    priceHidden = document.getElementById('productHidden');
    priceHTML = document.getElementsByClassName('price');

    jQuery('form[name=customDataForm]').on('submit', function(e) {
        e.preventDefault();
    });

    jQuery('.select-item').click(function(e) {
        var parentSelect = jQuery(this).parent().attr('id');
        var childrenSelect = jQuery('#' + parentSelect).children();
        jQuery(childrenSelect).each(function() {
            jQuery(this).removeClass('select-item-checked');
        });
        jQuery(this).addClass('select-item-checked');
    });
}

function changeProductPrice(element) {
    jQuery.ajax({
        type: 'POST',
        url: custom_admin_url.ajax_url,
        data: {
            action: 'getpricetable',
            data: jQuery('form[name=customDataForm]').serialize(),
            id: jQuery('#productID').val()
        },
        success: function(response) {
            var respuesta = JSON.parse(response);
            var json_data = respuesta['array_vars'];
            var append = "";
            jQuery.each(json_data, function(key, value) {
                append += key + ':' + value + ',';
            });

            jQuery('#productHidden').val(respuesta['price']);
            jQuery('.price').html(respuesta['price_html']);
            jQuery('#productVars').val(append);

            if (respuesta['photo_change'] != '') {
                jQuery('.woocommerce-custom-image-container').empty();
                jQuery('.woocommerce-custom-image-container').html('<img class="custom-change" src="' + custom_admin_url.template_url + '/images/layers-test/' + respuesta['photo_change'] + '" />');
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            reject(error);
        }
    });
}


document.addEventListener("DOMContentLoaded", documentCustomLoad, false);

(function($) {

    $(document).on('click', '.single_add_to_cart_button', function(e) {
        e.preventDefault();

        var $thisbutton = $(this),
            $form = $thisbutton.closest('form.cart'),
            id = $thisbutton.val(),
            product_qty = $form.find('input[name=quantity]').val() || 1,
            product_id = $form.find('input[name=product_id]').val() || id,
            variation_id = $form.find('input[name=variation_id]').val() || 0,
            product_price = $form.find('input[name=product_price]').val() || 0,
            product_vars = $form.find('input[name=product_vars]').val() || 0;

        var data = {
            action: 'woocommerce_ajax_add_to_cart',
            product_id: product_id,
            product_price: product_price,
            product_vars: product_vars,
            product_sku: '',
            quantity: product_qty,
            variation_id: variation_id,
        };

        $(document.body).trigger('adding_to_cart', [$thisbutton, data]);

        $.ajax({
            type: 'post',
            url: wc_add_to_cart_params.ajax_url,
            data: data,
            beforeSend: function(response) {

                $thisbutton.removeClass('added').addClass('loading');
            },
            complete: function(response) {
                jQuery('#cartModal').modal('show');
                $thisbutton.addClass('added').removeClass('loading');
            },
            success: function(response) {

                if (response.error && response.product_url) {
                    window.location = response.product_url;
                    return;
                } else {
                    $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, $thisbutton]);
                }
            },
        });

        return false;
    });
})(jQuery);

jQuery('#resetForms').on('click', function(e) {
    e.preventDefault();

    var $form = jQuery('form.cart');
    jQuery('.single_add_to_cart_button').removeClass('added');

    jQuery('.added_to_cart').remove();

    jQuery('input[name=product_price]').val(0);
    jQuery('input[name=product_vars]').val('');
    $form.trigger("reset");

    jQuery('form[name=customDataForm]').trigger('reset');

    jQuery('#accordionExample .collapse').collapse('hide');
    jQuery('#collapseOne').addClass('show');
    jQuery('#headingOne h2').removeClass('collapsed');
    jQuery('#cartModal').modal('hide');

});