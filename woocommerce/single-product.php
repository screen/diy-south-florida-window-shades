<?php

/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

get_header('shop'); ?>

<?php
/**
 * woocommerce_before_main_content hook.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 */
do_action('woocommerce_before_main_content');
?>
<div class="customizer-container-wrapper container">
    <div class="row">
        <form name="customDataForm" class="customizer-content-wrapper col-8">
            <div class="accordion custom-accordion" id="accordionExample">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <button class="btn btn-link btn-block btn-accordion text-left">
                                <?php _e('Step 1: Measure your windows', 'diy'); ?> <div class="plus-symbol"><span></span><span></span></div>
                            </button>
                        </h2>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                            <a type="button" class="text-link" data-toggle="modal" data-target="#reviewModal">
                                <?php _e('Review the correct method of measuring your window dimensions', 'diyflorida'); ?>
                            </a>
                            <div class="w-100"></div>
                            <a type="button" class="text-link" data-toggle="modal" data-target="#videoModal">
                                <i class="fa fa-video-camera"></i><?php _e('Watch the video tutorial', 'diyflorida'); ?>
                            </a>

                            <div class="form-inline-product-data-container">
                                <div class="form-inline-item">
                                    <label for="pa_room"><?php _e('Room', 'diyflorida'); ?></label>
                                    <?php $arr_room = get_custom_product_attributes('pa_room'); ?>
                                    <select class="form-control" name="pa_room" id="pa_room">
                                        <?php foreach ($arr_room as $key => $value) { ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-inline-item">
                                    <label for="pa_width"><?php _e('Width', 'diyflorida'); ?></label>
                                    <?php $arr_width = get_custom_product_attributes('pa_width'); ?>
                                    <div class="form-multiple-inline">
                                        <select class="form-control" name="pa_width" id="pa_width" onChange="changeProductPrice(this);">
                                            <?php foreach ($arr_width as $key => $value) { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        </select>
                                        <select name="pa_extra_width" id="pa_extra_width" class="form-control">
                                            <option value="1/8">1/8"</option>
                                            <option value="2/8">2/8"</option>
                                            <option value="3/8">3/8"</option>
                                            <option value="4/8">4/8"</option>
                                            <option value="5/8">5/8"</option>
                                            <option value="6/8">6/8"</option>
                                            <option value="7/8">7/8"</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-inline-item">
                                    <label for="pa_height"><?php _e('Height', 'diyflorida'); ?></label>
                                    <div class="form-multiple-inline">
                                        <?php $arr_height = get_custom_product_attributes('pa_height'); ?>
                                        <select class="form-control" name="pa_height" id="pa_height" onChange="changeProductPrice(this);">
                                            <?php foreach ($arr_height as $key => $value) { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        </select>
                                        <select name="pa_extra_height" id="pa_extra_height" class="form-control">
                                            <option value="1/8">1/8"</option>
                                            <option value="2/8">2/8"</option>
                                            <option value="3/8">3/8"</option>
                                            <option value="4/8">4/8"</option>
                                            <option value="5/8">5/8"</option>
                                            <option value="6/8">6/8"</option>
                                            <option value="7/8">7/8"</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-baseboard-selector">
                                <label for="product_baseboard"><input type="checkbox" id="product_baseboard" name="product_baseboard"> <?php _e('With baseboard') ?></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h2 class="mb-0 collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                            <button class="btn btn-link btn-block btn-accordion text-left">
                                <?php _e('Step 2: Choose a Fabric Type', 'diy'); ?> <div class="plus-symbol"><span></span><span></span></div>
                            </button>
                        </h2>
                    </div>

                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            <div id="selectFabricType" class="select-custom-container">
                                <?php $arr_fabric = get_custom_product_attributes('pa_fabric-type'); ?>
                                <?php foreach ($arr_fabric as $key => $value) { ?>
                                <?php echo get_selective_box($key, $value, 'pa_fabric-type'); ?>
                                <?php } ?>
                            </div>
                            <div class="facsia-container">
                                <h2><?php _e('Add a Fascia?', 'diyflorida'); ?></h2>
                                <div id="selectFascia" class="select-custom-color-container">
                                    <?php $arr_fabric = get_custom_product_attributes('pa_fascia'); ?>
                                    <?php foreach ($arr_fabric as $key => $value) { ?>
                                    <?php echo get_selective_box($key, $value, 'pa_fascia'); ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h2 class="mb-0 collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                            <button class="btn btn-link btn-block btn-accordion text-left">
                                <?php _e('Step 3: Choose a Color', 'diy'); ?> <div class="plus-symbol"><span></span><span></span></div>
                            </button>
                        </h2>
                    </div>

                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body">
                            <div id="selectWindowColor" class="select-custom-color-container">
                                <?php $arr_fabric = get_custom_product_attributes('pa_window-color'); ?>
                                <?php foreach ($arr_fabric as $key => $value) { ?>
                                <?php echo get_selective_box($key, $value, 'pa_window-color'); ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingFour">
                        <h2 class="mb-0 collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                            <button class="btn btn-link btn-block btn-accordion text-left">
                                <?php _e('Step 4: Choose a Side Guide', 'diy'); ?> <div class="plus-symbol"><span></span><span></span></div>
                            </button>
                        </h2>
                    </div>

                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                        <div class="card-body">
                            <div id="selectSideGuideColor" class="select-custom-color-container">
                                <?php $arr_fabric = get_custom_product_attributes('pa_side-guide'); ?>
                                <?php foreach ($arr_fabric as $key => $value) { ?>
                                <?php echo get_selective_box($key, $value, 'pa_side-guide'); ?>
                                <?php } ?>
                            </div>
                            <h2>Control Side</h2>
                            <div id="selectSideGuide" class="select-custom-container select-control-side-container">
                                <?php $arr_fabric = get_custom_product_attributes('pa_control-side'); ?>
                                <?php foreach ($arr_fabric as $key => $value) { ?>
                                <?php echo get_selective_box($key, $value, 'pa_control-side'); ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingFive">
                        <h2 class="mb-0 collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                            <button class="btn btn-link btn-block btn-accordion text-left">
                                <?php _e('Step 5: Select a Mount Style', 'diy'); ?> <div class="plus-symbol"><span></span><span></span></div>
                            </button>
                        </h2>
                    </div>

                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                        <div class="card-body">
                            <div id="selectMountStyle" class="select-custom-container select-control-side-container">
                                <?php $arr_fabric = get_custom_product_attributes('pa_mount-style'); ?>
                                <?php foreach ($arr_fabric as $key => $value) { ?>
                                <?php echo get_selective_box($key, $value, 'pa_mount-style'); ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingSix">
                        <h2 class="mb-0 collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                            <button class="btn btn-link btn-block btn-accordion text-left">
                                <?php _e('Step 6: Do you want Motorized Shades?', 'diy'); ?> <div class="plus-symbol"><span></span><span></span></div>
                            </button>
                        </h2>
                    </div>

                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="select-motorized-radio-container">
                                <label for=""><input type="radio" name="motorized" value="yes"> <?php _e('Yes', 'diyflorida'); ?></label>
                                <label for=""><input type="radio" name="motorized" value="no"> <?php _e('No', 'diyflorida'); ?></label>
                            </div>
                            <H2>MOTOR BRAND</H2>
                            <div id="selectBrand" class="select-custom-container select-brand-container">
                                <?php $arr_fabric = get_custom_product_attributes('pa_motor-brand'); ?>
                                <?php foreach ($arr_fabric as $key => $value) { ?>
                                <?php echo get_selective_box($key, $value, 'pa_motor-brand'); ?>
                                <?php } ?>
                            </div>
                            <H2>TYPE OF MOTOR</H2>
                            <div id="selectMotor" class="select-custom-container select-brand-container">
                                <?php $arr_fabric = get_custom_product_attributes('pa_type-of-motor'); ?>
                                <?php foreach ($arr_fabric as $key => $value) { ?>
                                <?php echo get_selective_box($key, $value, 'pa_type-of-motor'); ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
        <div class="customizer-product-wrapper col-4">
            <?php while (have_posts()) : ?>
            <?php the_post(); ?>
            <?php wc_get_template_part('content', 'single-product'); ?>
            <?php endwhile; // end of the loop. 
            ?>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal modal-inst fade" id="reviewModal" tabindex="-1" aria-labelledby="reviewModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="modal-inst-title col-12">
                            <h2><?php _e('How to measure your windows', 'diyflorida'); ?></h2>
                        </div>
                        <div class="modal-inst-item col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/inst01.png" alt="Instructions 1" class="img-fluid" />
                            <p>Measure the width of area to be covered in 3 places: For inside mounting, use the TOP measurement. If the middle and bottom measurements are smaller by MORE than 1/4 of an inch, please contact us via chat, phone or email before ordering to help you. For outside mounting, please add an additional 1/2 inch on EACH side of the opening (if available - if not, please make sure you have at least 4 inches of additional space above the open). Email us if you have anyquestions: please include pictures and a breif description of your questions.</p>
                        </div>
                        <div class="modal-inst-item col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/inst02.png" alt="Instructions 2" class="img-fluid" />
                            <p>Measure the total height of area to be covered in 3 places. Use the largest measurement.</p>
                        </div>
                        <div class="modal-inst-item col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/inst03.png" alt="Instructions 3" class="img-fluid" />
                            <p>Some large windows that reach the floor may have a baseboard. Please take that into consideration and mark the WITH BASEBOARD check if needed. Email us if you have any questions. Please include pictures and a brief description of your questions.</p>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal modal-video fade" id="videoModal" tabindex="-1" aria-labelledby="videoModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-body">
                <div class="container-fluid p-0">
                    <div class="row no-gutters">
                        <div class="video-container col-12">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/S7SLep244ss?rel=0" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal modal-cart fade" id="cartModal" tabindex="-1" aria-labelledby="cartModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row align-items-center justify-content-center">
                        <div class="modal-cart-content col-12">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon_added.png" alt="Added to Cart" class="img-fluid" />
                            <h2><?php _e('Your Items have been added to your cart', 'diyflorida'); ?></h2>
                            <div class="cart-buttons-container">
                                <a id="resetForms" class="btn btn-md btn-modal-cart"><?php _e('Continue shopping shades', 'diyflorida'); ?></a>
                                <a id="interWindow" class="btn btn-md btn-modal-cart"><?php _e('Finish your order', 'diyflorida'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
/**
 * woocommerce_after_main_content hook.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action('woocommerce_after_main_content');
?>
<?php
get_footer('shop');

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */