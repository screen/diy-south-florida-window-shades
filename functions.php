<?php

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER CSS
-------------------------------------------------------------- */

require_once('includes/wp_enqueue_styles.php');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER JS
-------------------------------------------------------------- */

if (!is_admin()) add_action('wp_enqueue_scripts', 'diyflorida_jquery_enqueue');
function diyflorida_jquery_enqueue()
{
    wp_deregister_script('jquery');
    wp_deregister_script('jquery-migrate');
    if ($_SERVER['REMOTE_ADDR'] == '::1') {
        /*- JQUERY ON LOCAL  -*/
        wp_register_script('jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '3.5.1', false);
        /*- JQUERY MIGRATE ON LOCAL  -*/
        wp_register_script('jquery-migrate', get_template_directory_uri() . '/js/jquery-migrate.min.js',  array('jquery'), '3.1.0', false);
    } else {
        /*- JQUERY ON WEB  -*/
        wp_register_script('jquery', 'https://code.jquery.com/jquery-3.5.1.min.js', false, '3.5.1', false);
        /*- JQUERY MIGRATE ON WEB  -*/
        wp_register_script('jquery-migrate', 'https://code.jquery.com/jquery-migrate-3.3.1.js', array('jquery'), '3.3.1', true);
    }
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-migrate');
}

/* NOW ALL THE JS FILES */

require_once('includes/wp_enqueue_scripts.php');

/* --------------------------------------------------------------
    ADD CUSTOM WALKER BOOTSTRAP
-------------------------------------------------------------- */

add_action('after_setup_theme', 'diyflorida_register_navwalker');
function diyflorida_register_navwalker()
{
    require_once('includes/class-wp-bootstrap-navwalker.php');
}

/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_custom_functions.php');

/* --------------------------------------------------------------
    ADD REQUIRED WORDPRESS PLUGINS
-------------------------------------------------------------- */

require_once('includes/class-tgm-plugin-activation.php');
require_once('includes/class-required-plugins.php');

/* --------------------------------------------------------------
    ADD CUSTOM WOOCOMMERCE OVERRIDES
-------------------------------------------------------------- */
if (class_exists('WooCommerce')) {
    require_once('includes/wp_woocommerce_functions.php');
}

/* --------------------------------------------------------------
    ADD JETPACK COMPATIBILITY
-------------------------------------------------------------- */
if (defined('JETPACK__VERSION')) {
    require_once('includes/wp_jetpack_functions.php');
}

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */

register_nav_menus(array(
    'header_menu' => __('Menu Header - Principal', 'diyflorida')
));

/* --------------------------------------------------------------
    ADD DYNAMIC SIDEBAR SUPPORT
-------------------------------------------------------------- */

add_action('widgets_init', 'diyflorida_widgets_init');

function diyflorida_widgets_init()
{
    register_sidebar(array(
        'name' => __('Sidebar Principal', 'diyflorida'),
        'id' => 'main_sidebar',
        'description' => __('Estos widgets seran vistos en las entradas y páginas del sitio', 'diyflorida'),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ));

    register_sidebars(3, array(
        'name'          => __('Pie de Página %d', 'diyflorida'),
        'id'            => 'sidebar_footer',
        'description'   => __('Estos widgets seran vistos en el pie de página del sitio', 'diyflorida'),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>'
    ));

    //    register_sidebar( array(
    //        'name' => __( 'Sidebar de la Tienda', 'diyflorida' ),
    //        'id' => 'shop_sidebar',
    //        'description' => __( 'Estos widgets seran vistos en Tienda y Categorias de Producto', 'diyflorida' ),
    //        'before_widget' => '<li id='%1$s' class='widget %2$s'>',
    //        'after_widget'  => '</li>',
    //        'before_title'  => '<h2 class='widgettitle'>',
    //        'after_title'   => '</h2>',
    //    ) );
}



/* --------------------------------------------------------------
    ADD CUSTOM METABOX
-------------------------------------------------------------- */

require_once('includes/wp_custom_metabox.php');

/* --------------------------------------------------------------
    ADD CUSTOM POST TYPE
-------------------------------------------------------------- */

require_once('includes/wp_custom_post_type.php');

/* --------------------------------------------------------------
    ADD CUSTOM THEME CONTROLS
-------------------------------------------------------------- */

require_once('includes/wp_custom_theme_control.php');

/* --------------------------------------------------------------
    ADD CUSTOM IMAGE SIZE
-------------------------------------------------------------- */
if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(9999, 400, true);
}
if (function_exists('add_image_size')) {
    add_image_size('avatar', 100, 100, true);
    add_image_size('blog_img', 276, 217, true);
    add_image_size('single_img', 636, 297, true);
}

/*
add_action('wp_ajax_get_attributes_prices', 'ajax_get_attributes_prices_handler');

function ajax_get_attributes_prices_handler()
{
    global $wpdb; 
    $arrData = $wpdb->get_results("SELECT term_id FROM $wpdb->term_taxonomy WHERE taxonomy = 'pa_height'", ARRAY_A);
    foreach ($arrData as $item) { 
    $varTerm = get_term_by('ID', $item['term_id'], 'pa_height');
    $arrHeight[] = $varTerm->name;
    }

    $arrData = $wpdb->get_results("SELECT term_id FROM $wpdb->term_taxonomy WHERE taxonomy = 'pa_width'", ARRAY_A); 
    foreach ($arrData as $item) { 
    $varTerm = get_term_by('ID', $item['term_id'], 'pa_width');
    $arrWidth[] = $varTerm->name;
    }
    
    foreach ($arrWidth as $key => $value) {
        $i = 0;
        foreach ($arrHeight as $item) {
            $masterData[$value][$item] = $item;
        $i++; }
    } 

    
    $response = json_encode($masterData, JSON_FORCE_OBJECT);
    echo $response;
    wp_die();
}
*/

add_action('wp_ajax_get_attributes_prices_width', 'ajax_get_attributes_prices_width_handler');

function ajax_get_attributes_prices_width_handler()
{
    global $wpdb;
    $arrMaster = array();
    $arrData = $wpdb->get_results("SELECT term_id FROM $wpdb->term_taxonomy WHERE taxonomy = 'pa_width'", ARRAY_A);
    $i = 0;
    foreach ($arrData as $item) {
        $varTerm = get_term_by('ID', $item['term_id'], 'pa_width');
        $menu_order = get_term_meta($item['term_id'], 'order', true);
        if ($menu_order == '') {
            $menu_order = $i;
        }
        $arraySort[$menu_order] = $varTerm->term_id;
        $i++;
    }

    ksort($arraySort);

    foreach ($arraySort as $item) {
        $varTerm = get_term_by('ID', $item, 'pa_width');
        $arrWidth[] = $varTerm->name;
    }

    foreach ($arrWidth as $item) {
        $masterData[] = $item;
    }

    $response = json_encode($masterData);
    return $response;
    wp_die();
}

add_action('wp_ajax_get_attributes_prices_height', 'ajax_get_attributes_prices_height_handler');

function ajax_get_attributes_prices_height_handler()
{
    global $wpdb;
    $arrMaster = array();
    $arrData = $wpdb->get_results("SELECT term_id FROM $wpdb->term_taxonomy WHERE taxonomy = 'pa_height'", ARRAY_A);
    $i = 0;
    foreach ($arrData as $item) {
        $varTerm = get_term_by('ID', $item['term_id'], 'pa_height');
        $menu_order = get_term_meta($item['term_id'], 'order', true);
        if ($menu_order == '') {
            $menu_order = $i;
        }
        $arraySort[$menu_order] = $varTerm->term_id;
        $i++;
    }

    ksort($arraySort);

    foreach ($arraySort as $item) {
        $varTerm = get_term_by('ID', $item, 'pa_height');
        $arrHeight[] = $varTerm->name;
    }

    foreach ($arrHeight as $item) {
        $masterData[] = $item;
    }

    $response = json_encode($masterData);
    return $response;
    wp_die();
}

add_action('wp_ajax_save_table_data', 'save_table_data_handler');

function save_table_data_handler()
{
    $info = $_POST['info'];
    $table = $_POST['table'];
    update_option('prices_table_' . $table, $info);

    wp_die();
}

add_action('wp_ajax_save_sideguide_data', 'save_sideguide_data_callback');

function save_sideguide_data_callback()
{
    $info = $_POST['info'];
    update_option('prices_sideguide', $info);
    wp_die();
}

function get_matrix_price($width, $height, $fabric)
{
    if (defined('WP_DEBUG') && WP_DEBUG) {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
    }
    global $wpdb;
    $arraySort = array();
    $arrData = $wpdb->get_results("SELECT term_id FROM $wpdb->term_taxonomy WHERE taxonomy = 'pa_width'", ARRAY_A);
    $i = 0;
    foreach ($arrData as $item) {
        $varTerm = get_term_by('ID', $item['term_id'], 'pa_width');
        $menu_order = get_term_meta($item['term_id'], 'order', true);
        if ($menu_order == '') {
            $menu_order = $i;
        }
        $arraySort[$menu_order] = $varTerm->term_id;
        $i++;
    }

    ksort($arraySort);
    $i = 0;

    foreach ($arraySort as $item) {
        $varTerm = get_term_by('ID', $item, 'pa_width');
        $arrWidth[$i] = $varTerm->term_id;
        $i++;
    }

    $key_width = array_search($width, $arrWidth);

    $arraySort = array();
    $arrData = $wpdb->get_results("SELECT term_id FROM $wpdb->term_taxonomy WHERE taxonomy = 'pa_height'", ARRAY_A);
    $i = 0;
    foreach ($arrData as $item) {
        $varTerm = get_term_by('ID', $item['term_id'], 'pa_height');
        $menu_order = get_term_meta($item['term_id'], 'order', true);
        if ($menu_order == '') {
            $menu_order = $i;
        }
        $arraySort[$menu_order] = $varTerm->term_id;
        $i++;
    }

    ksort($arraySort);
    $i = 0;

    foreach ($arraySort as $item) {
        $varTerm = get_term_by('ID', $item, 'pa_height');
        $arrHeight[$i] = $varTerm->term_id;
        $i++;
    }

    $key_height = array_search($height, $arrHeight);

    /* FABRIC TYPE */
    if ($fabric == '') {
        $unfiltered_matrix = get_option('prices_table_1');
    } else {
        $varTerm = get_term_by('ID', $fabric, 'pa_fabric-type');
        if (($varTerm->name == '1%') || ($varTerm->name == 'Blackout')) {

            $unfiltered_matrix = get_option('prices_table_2');
        } else {
            $unfiltered_matrix = get_option('prices_table_1');
        }
    }

    $unfiltered_matrix = str_replace('\"', '', $unfiltered_matrix);
    $matrix = json_decode($unfiltered_matrix);

    $price_table = $matrix[$key_height][$key_width];

    return (int) $price_table;
}

function get_matrix_fascia_price($width, $height, $fascia)
{
    if (defined('WP_DEBUG') && WP_DEBUG) {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
    }
    global $wpdb;
    $arraySort = array();
    $arrData = $wpdb->get_results("SELECT term_id FROM $wpdb->term_taxonomy WHERE taxonomy = 'pa_width'", ARRAY_A);
    $i = 0;
    foreach ($arrData as $item) {
        $varTerm = get_term_by('ID', $item['term_id'], 'pa_width');
        $menu_order = get_term_meta($item['term_id'], 'order', true);
        if ($menu_order == '') {
            $menu_order = $i;
        }
        $arraySort[$menu_order] = $varTerm->term_id;
        $i++;
    }

    ksort($arraySort);
    $i = 0;

    foreach ($arraySort as $item) {
        $varTerm = get_term_by('ID', $item, 'pa_width');
        $arrWidth[$i] = $varTerm->term_id;
        $i++;
    }

    $key_width = array_search($width, $arrWidth);

    $arraySort = array();
    $arrData = $wpdb->get_results("SELECT term_id FROM $wpdb->term_taxonomy WHERE taxonomy = 'pa_height'", ARRAY_A);
    $i = 0;
    foreach ($arrData as $item) {
        $varTerm = get_term_by('ID', $item['term_id'], 'pa_height');
        $menu_order = get_term_meta($item['term_id'], 'order', true);
        if ($menu_order == '') {
            $menu_order = $i;
        }
        $arraySort[$menu_order] = $varTerm->term_id;
        $i++;
    }

    ksort($arraySort);
    $i = 0;

    foreach ($arraySort as $item) {
        $varTerm = get_term_by('ID', $item, 'pa_height');
        $arrHeight[$i] = $varTerm->term_id;
        $i++;
    }

    $key_height = array_search($height, $arrHeight);

    /* CHECK FASCIA */
    $unfiltered_matrix = get_option('prices_table_3');
    $unfiltered_matrix = str_replace('\"', '', $unfiltered_matrix);
    $matrix = json_decode($unfiltered_matrix);

    $varTerm = get_term_by('ID', $fascia, 'pa_fascia');
    if ($varTerm->name == 'None') {
        $price_fascia = 0;
    } else {
        if ($key_height > 6) {
            $price_fascia = $matrix[0][$key_width];
        } else {
            $price_fascia = $matrix[1][$key_width];
        }
    }

    return $price_fascia;
}

add_action('wp_ajax_getpricetable', 'getpricetable_callback');
add_action('wp_ajax_nopriv_getpricetable', 'getpricetable_callback');

function getpricetable_callback()
{
    if (defined('WP_DEBUG') && WP_DEBUG) {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
    }

    parse_str($_POST['data'], $info);
    $postid = $_POST['id'];

    if (isset($info['pa_fabric-type'])) {
        $fabric = $info['pa_fabric-type'];
    } else {
        $fabric = '';
    }

    if ((isset($info['pa_fabric-type'])) &&  (isset($info['pa_window-color']))) {
        $temp_type = get_term_by('id', $info['pa_fabric-type'], 'pa_fabric-type');
        $temp_color = get_term_by('id', $info['pa_window-color'], 'pa_window-color');

        $photo_name = 'fabric__' . $temp_color->slug . '-' . $temp_type->slug . '.png';
    } else {
        $photo_name = '';
    }

    /* GET MATRIX BY WIDTH - HEIGHT - FABRIC TYPE */
    $price = get_matrix_price($info['pa_width'], $info['pa_height'], $fabric);

    if (isset($info['pa_fascia'])) {
        $price_fascia = get_matrix_fascia_price($info['pa_width'], $info['pa_height'], $info['pa_fascia']);
    } else {
        $price_fascia = 0;
    }


    /* CUSTOM PRICES FOR MOTORIZED SHEETS */
    if (isset($info['motorized'])) {
        if ($info['motorized'] == 'yes') {
            if (isset($info['pa_motor-brand'])) {
                if (isset($info['pa_type-of-motor'])) {
                    $varTerm = get_term_by('ID', $info['pa_type-of-motor'], 'pa_type-of-motor');
                    $price_motorized = get_post_meta($postid, 'addon_' . $info['pa_motor-brand'] . '_' . $varTerm->slug, true);
                } else {
                    $price_motorized = 0;
                }
            } else {
                $price_motorized = 0;
            }
        } else {
            $price_motorized = 0;
        }
    } else {
        $price_motorized = 0;
    }


    $final_price = $price + $price_fascia + $price_motorized;

    ob_start();
?>
    <span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">$</span><?php echo number_format($final_price, 2, '.', ','); ?></bdi></span>
<?php
    $content = ob_get_clean();
    $final_response = array(
        'price' => $final_price,
        'price_html' => $content,
        'array_vars' => $info,
        'photo_change' => $photo_name
    );

    echo json_encode($final_response);
    wp_die();
}
