<?php

/**
 * Template Name: One Last Thing
 */

get_header();
?>
<div class="container one-last-thing-container">
    <div class="row">
        <div class="col-12">
            <div class="section-title">
                <h2>One Last Thing...</h2>
                <p>You are almost done! Review your order and add the remotes and battery chargers needed for each room.</p>
            </div>
        </div>

        <div class="col-12">
            <?php
            $data = [];

            // Loop over $cart items
            foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
                $room_id = $cart_item['product_vars']['pa_room'];

                if (empty($data[$room_id])) {
                    $data[$room_id]['term'] = get_term_by('ID', $room_id, 'pa_room');
                    $data[$room_id]['quantity'] = 0;
                }

                $data[$room_id]['quantity'] +=  $cart_item['quantity'];
            }


            foreach ($data as $id => $additionals) {
                if ($additionals['quantity'] == 0) {
                    continue;
                }
            ?>
                <div class="additional-item-row">
                    <div class="additional-item-row__title">
                        <span class="text-uppercase"><?php echo $additionals['term']->name ?></span>
                        <span class="text-muted">(<?php printf("%d %s added in total", $additionals['quantity'], _n('shade', 'shades', $additionals['quantity'])) ?>)</span>
                    </div>

                    <div class="additional-item-row__content">
                        <form>
                            <div class="form-row">
                                <div class="col-4">
                                    <label for="<?php echo $id ?>remote-qty">How many remotes do you need for this room?</label>
                                    <input type="number" class="form-control" id="<?php echo $id ?>remote-qty" value="1">
                                </div>
                                <div class="col-3">
                                    <label for="<?php echo $id ?>type">Select a type of remote</label>
                                    <select class="custom-select" id="<?php echo $id ?>type">
                                        <option selected>Open this select menu</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <div class="col">
                                    <label for="<?php echo $id ?>batt-qty">How many motor battery chargers do you need for this room?</label>
                                    <input type="number" class="form-control" id="<?php echo $id ?>batt-qty" value="1">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
        <div class="col-12 d-flex flex-row-reverse">
            <a href="#" class="btn btn-lg btn-warning text-uppercase">Continue to your cart</a>
        </div>
    </div>
</div>
<?php
get_footer();
