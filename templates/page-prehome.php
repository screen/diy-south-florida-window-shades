<?php

/**
 * Template Name: Pre-Home
 *
 * @package diyflorida
 * @subpackage diyflorida-mk01-theme
 * @since Mk. 1.0
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">

        <section class="two-side-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fliud">
                <div class="row no-gutters">
                    <?php $arr_hero = get_post_meta(get_the_ID(), 'diy_home_slider_group', true); ?>
                    <?php if (!empty($arr_hero)) { ?>
                        <?php $i = 1; ?>
                        <?php foreach ($arr_hero as $item) { ?>
                            <?php $delay = 250 * $i; ?>
                            <article class="two-side-item side-<?php echo $i; ?> col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <picture>
                                    <?php $bg_banner = wp_get_attachment_image_src($item['bg_image_id'], 'full', false); ?>
                                    <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($item['bg_image_id'], '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($item['bg_image_id'], '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" data-aos="fade" data-aos-delay="<?php echo $delay; ?>" />
                                </picture>
                                <div class="two-side-item-wrapper">
                                    <h4 data-aos="fade" data-aos-delay="<?php echo $delay + 50; ?>"><?php echo $item['pretitle']; ?></h4>
                                    <h2 data-aos="fade" data-aos-delay="<?php echo $delay + 60; ?>"><?php echo $item['title']; ?></h2>
                                    <a href="<?php echo $item['button_link']; ?>" class="btn btn-md btn-twoside" data-aos="fade" data-aos-delay="<?php echo $delay + 380; ?>"><?php echo $item['button_text']; ?></a>
                                    <h5 data-aos="fade" data-aos-delay="<?php echo $delay + 400; ?>"><?php echo $item['button_posttitle']; ?></h5>
                                </div>
                                <?php if ($i == 1) { ?>
                                    <div class="two-side-middle">
                                        <?php _e('OR', 'diyflorida'); ?>
                                    </div>
                                <?php } ?>
                            </article>
                        <?php $i++;
                        } ?>
                    <?php } ?>
                </div>
            </div>
        </section>

        <section class="image-side-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <article class="content-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="50">
                        <div class="wrapper">
                            <h2 data-aos="fade" data-aos-delay="50"><?php echo get_post_meta(get_the_ID(), 'diy_twosided_title', true); ?></h2>
                            <?php $arr_group = get_post_meta(get_the_ID(), 'diy_twosided_group', true); ?>
                            <?php if (!empty($arr_group)) { ?>
                                <?php $i = 1; ?>
                                <?php foreach ($arr_group as $item) { ?>
                                    <?php $delay = 250 * $i; ?>
                                    <div class="group-wrapper" data-aos="fade" data-aos-delay="<?php echo $delay; ?>">
                                        <div class="media">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon_checkmark.png" alt="Checkmark" class="img-fluid align-self-top">
                                            <div class="media-body"> 
                                                <h3><?php echo $item['title']; ?></h3>
                                                <?php echo apply_filters('the_content', $item['desc']); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php $i++; } ?>
                            <?php } ?>
                        </div>
                        <picture>
                            <?php $bg_banner_id = get_post_meta(get_the_ID(), 'diy_twosided_image_id', true); ?>
                            <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                            <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" data-aos="fade" data-aos-delay="700" />
                        </picture>
                    </article>
                </div>
            </div>
        </section>

        <section class="image-side-section image-left col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <article class="content-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="50">
                        <picture>
                            <?php $bg_banner_id = get_post_meta(get_the_ID(), 'diy_twosided2_image_id', true); ?>
                            <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                            <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" data-aos="fade" data-aos-delay="150" />
                        </picture>
                        <div class="wrapper">
                            <h2 data-aos="fade" data-aos-delay="50"><?php echo get_post_meta(get_the_ID(), 'diy_twosided2_title', true); ?></h2>
                            <?php $arr_group = get_post_meta(get_the_ID(), 'diy_twosided2_group', true); ?>
                            <?php if (!empty($arr_group)) { ?>
                                <?php $i = 1; ?>
                                <?php foreach ($arr_group as $item) { ?>
                                    <?php $delay = 250 * $i; ?>
                                    <div class="group-wrapper" data-aos="fade" data-aos-delay="<?php echo $delay; ?>">
                                        <div class="media">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon_checkmark.png" alt="Checkmark" class="img-fluid align-self-top">
                                            <div class="media-body">
                                                <h3><?php echo $item['title']; ?></h3>
                                                <?php echo apply_filters('the_content', $item['desc']); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </article>
                </div>
            </div>
        </section>

    </div>
</main>
<?php get_footer(); ?>