<?php
global $wpdb;
$product_attributes = array();

$arrData = $wpdb->get_results("SELECT taxonomy FROM $wpdb->term_taxonomy WHERE taxonomy LIKE '%pa_%'", ARRAY_A);
foreach ($arrData as $item) {
    if (!in_array($item['taxonomy'], $product_attributes)) {
        $product_attributes[] = $item['taxonomy'];
    }
}

/* --------------------------------------------------------------
    1.- TERMS: PRODUCT ATTRIBUTES
-------------------------------------------------------------- */
$cmb_terms = new_cmb2_box(array(
    'id'            => $prefix . 'terms',
    'title'         => esc_html__('Atributos: Información Extra', 'diyflorida'),
    'object_types'  => array('term'),
    'taxonomies'      => $product_attributes,
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true,
    'cmb_styles'    => true,
    'closed'        => false
));

$cmb_terms->add_field(array(
    'id'        => $prefix . 'activate_image',
    'name'      => esc_html__('Imagen en lugar de un color?', 'diyflorida'),
    'desc'      => esc_html__('Activar si necesita colocar una imagen en lugar de un color', 'diyflorida'),
    'type'      => 'checkbox'
));

$cmb_terms->add_field(array(
    'id'        => $prefix . 'term_color',
    'name'      => esc_html__('Color del Atributo', 'diyflorida'),
    'desc'      => esc_html__('Seleccione el color para este atributo', 'diyflorida'),
    'type'      => 'colorpicker',
    'default'   => '#ffffff'
));

$cmb_terms->add_field(array(
    'id'        =>  $prefix . 'term_image',
    'name'      => esc_html__('Imagen del Atributo', 'diyflorida'),
    'desc'      => esc_html__('Cargar una imagen para este atributo', 'diyflorida'),
    'type'      => 'file',
    'options'   => array(
        'url'   => false
    ),
    'text'      => array(
        'add_upload_file_text' => esc_html__('Cargar imagen', 'diyflorida'),
    ),
    'query_args' => array(
        'type'  => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'thumbnail'
));

$cmb_terms->add_field(array(
    'id'        =>  $prefix . 'change_image',
    'name'      => esc_html__('Imagen al Seleccionar', 'diyflorida'),
    'desc'      => esc_html__('Cargar una imagen que aparecerá si se selecciona el atributo', 'diyflorida'),
    'type'      => 'file',
    'options'   => array(
        'url'   => false
    ),
    'text'      => array(
        'add_upload_file_text' => esc_html__('Cargar imagen', 'diyflorida'),
    ),
    'query_args' => array(
        'type'  => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'thumbnail'
));
