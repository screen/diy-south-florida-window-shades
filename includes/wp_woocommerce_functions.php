<?php
/* WOOCOMMERCE CUSTOM COMMANDS */

/* WOOCOMMERCE - DECLARE THEME SUPPORT - BEGIN */
add_action('after_setup_theme', 'diyflorida_woocommerce_support');
function diyflorida_woocommerce_support()
{
    add_theme_support('woocommerce');
    //add_theme_support('wc-product-gallery-zoom');
    //add_theme_support('wc-product-gallery-lightbox');
    //add_theme_support('wc-product-gallery-slider');
}
/* WOOCOMMERCE - DECLARE THEME SUPPORT - END */

/* WOOCOMMERCE - CUSTOM WRAPPER - BEGIN */
remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'diyflorida_woocommerce_custom_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'diyflorida_woocommerce_custom_wrapper_end', 10);

function diyflorida_woocommerce_custom_wrapper_start()
{
    echo '<section id="main" class="container"><div class="row"><div class="woocustom-main-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">';
}

function diyflorida_woocommerce_custom_wrapper_end()
{
    echo '</div></div></section>';
}
/* WOOCOMMERCE - CUSTOM WRAPPER - END */

/* WOOCOMMERCE - ARCHIVE PRODUCT - START */
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
remove_action('woocommerce_cart_collaterals', 'woocommerce_cross_sell_display', 10);
/* WOOCOMMERCE - ARCHIVE PRODUCT - END */


/* WOOCOMMERCE - CUSTOM CONTENT PRODUCT - SHOP - START */
//remove_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10);
//remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5);
//remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
//remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
/* WOOCOMMERCE - CUSTOM CONTENT PRODUCT - SHOP - END */


/* WOOCOMMERCE - SINGLE PRODUCT - START */
remove_action('woocommerce_before_single_product', 'woocommerce_output_all_notices', 10);
remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20);
add_action('woocommerce_single_product_summary', 'woocommerce_show_product_images', 1);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 29);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
/* WOOCOMMERCE - SINGLE PRODUCT - END */

add_filter('woocommerce_product_data_tabs', 'add_custom_addons_data_tab', 99, 1);
function add_custom_addons_data_tab($product_data_tabs)
{
    $product_data_tabs['custom-addons-tab'] = array(
        'label' => __('Custom Addons', 'diyflorida'),
        'target' => 'custom_addons_data',
    );
    return $product_data_tabs;
}

add_action('woocommerce_product_data_panels', 'add_custom_addons_data_fields');
function add_custom_addons_data_fields()
{
    global $woocommerce, $post, $wpdb;
?>
    <!-- id below must match target registered in above add_custom_addons_data_tab function -->
    <div id="custom_addons_data" class="panel woocommerce_options_panel">
        <?php
        $arrData = $wpdb->get_results("SELECT term_id FROM $wpdb->term_taxonomy WHERE taxonomy = 'pa_motor-brand'", ARRAY_A);
        $i = 0;
        foreach ($arrData as $item) {
            $varTerm = get_term_by('ID', $item['term_id'], 'pa_motor-brand');
            $menu_order = get_term_meta($item['term_id'], 'order', true);
            if ($menu_order == '') {
                $menu_order = $i;
            }
            $arraySort[$menu_order] = $varTerm->term_id;
            $i++;
        }

        ksort($arraySort);
        $i = 0;

        foreach ($arraySort as $item) {
            $varTerm = get_term_by('ID', $item, 'pa_motor-brand');
            $arrBrand[$i] = array($varTerm->term_id, $varTerm->name);
            $i++;
        }

        foreach ($arrBrand as $item) {
        ?>
            <h2><?php echo $item[1]; ?></h2>
        <?php
            woocommerce_wp_text_input(array(
                'id'            => 'addon_' . $item[0] . '_wired',
                'wrapper_class' => 'show_if_simple',
                'label'         => __('Wired Price for: ', 'diyflorida') . $item[1],
                'placeholder'   => __('Add custom price for Wired Installation', 'diyflorida'),
                'default'       => '0',
                'desc_tip'      => false,
            ));

            woocommerce_wp_text_input(array(
                'id'            => 'addon_' . $item[0] . '_battery',
                'wrapper_class' => 'show_if_simple',
                'label'         => __('Battery Price for: ', 'diyflorida') . $item[1],
                'placeholder'   => __('Add custom price for Battery Installation', 'diyflorida'),
                'default'       => '0',
                'desc_tip'      => false,
            ));
        }
        ?>
    </div>
    <?php
}

add_action('woocommerce_process_product_meta', 'woocommerce_process_product_meta_fields_save');
function woocommerce_process_product_meta_fields_save($post_id)
{
    global $wpdb;
    $arrData = $wpdb->get_results("SELECT term_id FROM $wpdb->term_taxonomy WHERE taxonomy = 'pa_motor-brand'", ARRAY_A);
    $i = 0;
    foreach ($arrData as $item) {
        $varTerm = get_term_by('ID', $item['term_id'], 'pa_motor-brand');
        $menu_order = get_term_meta($item['term_id'], 'order', true);
        if ($menu_order == '') {
            $menu_order = $i;
        }
        $arraySort[$menu_order] = $varTerm->term_id;
        $i++;
    }

    ksort($arraySort);
    $i = 0;

    foreach ($arraySort as $item) {
        $varTerm = get_term_by('ID', $item, 'pa_motor-brand');
        $arrBrand[$i] = array($varTerm->term_id, $varTerm->name);
        $i++;
    }

    foreach ($arrBrand as $item) {
        if (isset($_POST['addon_' . $item[0] . '_battery'])) {
            update_post_meta($post_id, 'addon_' . $item[0] . '_battery', $_POST['addon_' . $item[0] . '_battery']);
        }

        if (isset($_POST['addon_' . $item[0] . '_wired'])) {
            update_post_meta($post_id, 'addon_' . $item[0] . '_wired', $_POST['addon_' . $item[0] . '_wired']);
        }
    }
}






add_action('woocommerce_before_add_to_cart_button', 'custom_hidden_product_field', 11);
function custom_hidden_product_field()
{
    $price = get_post_meta(get_the_ID(), '_regular_price', true);
    echo '<input type="hidden" id="productHidden" name="product_price" class="product_price" value="' . $price . '" />';
    echo '<input type="hidden" id="productID" name="product_id" class="product_id" value="' . get_the_ID() . '" />';
    echo '<input type="hidden" id="productVars" name="product_vars" class="product_id" value="" />';
}




function get_custom_product_attributes($attr_name)
{
    global $wpdb;
    $arrMaster = array();
    $arrData = $wpdb->get_results("SELECT term_id FROM $wpdb->term_taxonomy WHERE taxonomy = '{$attr_name}'", ARRAY_A);
    $i = 0;
    foreach ($arrData as $item) {
        $varTerm = get_term_by('ID', $item['term_id'], $attr_name);
        $menu_order = get_term_meta($item['term_id'], 'order', true);
        if ($menu_order == '') {

            $menu_order = $i;
        }
        $arraySort[$menu_order] = $varTerm->term_id;
        $i++;
    }

    ksort($arraySort);

    foreach ($arraySort as $item) {
        $varTerm = get_term_by('ID', $item, $attr_name);
        $arrMaster[$varTerm->term_id] = $varTerm->name;
    }

    return $arrMaster;
}


function get_selective_box($key, $value, $term)
{
    $activate = get_term_meta($key, 'diy_activate_image', true);
    if ($activate == 'on') {
    ?>
        <div class="select-item" onChange="changeProductPrice(this);">
            <label>
                <?php $image = get_term_meta($key, 'diy_term_image', true); ?>
                <img src="<?php echo $image; ?>" alt="<?php echo $value; ?>" class="img-fluid" />
                <?php echo $value; ?>
                <input type="radio" name="<?php echo $term; ?>" value="<?php echo $key; ?>" class="d-none" />
            </label>
        </div>
    <?php
    } else {
    ?>
        <div class="select-item select-item-color" onChange="changeProductPrice(this);">
            <label>
                <?php $color = get_term_meta($key, 'diy_term_color', true); ?>
                <?php if ($value == 'None') { ?>
                    <div class="color-selector none">
                        <div class="color-selector-wrapper"></div>
                    </div>
                <?php } else { ?>
                    <div class="color-selector">
                        <div class="color-selector-wrapper" style="background-color: <?php echo $color; ?>"></div>
                    </div>
                <?php } ?>
                <?php echo $value; ?>
                <input type="radio" name="<?php echo $term; ?>" value="<?php echo $key; ?>" class="d-none" />
            </label>
        </div>
    <?php
    }
}

function diy_add_engraving_text_to_cart_item($cart_item_data, $product_id, $variation_id)
{
    if (isset($_POST['product_vars'])) {
        $cart_item_data['product_id'] = $_POST['product_id'];
        $cart_item_data['product_price'] = $_POST['product_price'];

        $arr_vars = array();
        $custom_variables = explode(',', $_POST['product_vars']);

        foreach ($custom_variables as $item) {
            $temp = explode(':', $item);
            if ($temp[0] != '') {
                $arr_vars[$temp[0]] = $temp[1];
            }
        }
        $cart_item_data['product_vars'] = $arr_vars;

        return $cart_item_data;
    } else {
        return $cart_item_data;
    }
}

add_filter('woocommerce_add_cart_item_data', 'diy_add_engraving_text_to_cart_item', 10, 3);

add_filter('woocommerce_before_calculate_totals', 'custom_cart_items_prices', 10, 1);
function custom_cart_items_prices($cart)
{

    if (is_admin() && !defined('DOING_AJAX'))
        return;

    if (did_action('woocommerce_before_calculate_totals') >= 2)
        return;

    // Loop Through cart items
    $acc_fees = 0;
    foreach ($cart->get_cart() as $cart_item) {

        $price = $cart_item['data']->get_price();
        $quantity = $cart_item['quantity'];
        if (isset($cart_item['product_price'])) {
            $final_price = $cart_item['product_price'];
        } else {
            $final_price = $price;
        }

        // GET THE NEW PRICE (code to be replace by yours)
        $new_price = $final_price;

        // Updated cart item price
        $cart_item['data']->set_price($new_price);
    }
}


/* --------------------------------------------------------------
ADD CUSTOM DATA TO ITEM CART
-------------------------------------------------------------- */

add_filter('woocommerce_get_item_data', 'wc_add_info_to_cart', 10, 2);
function wc_add_info_to_cart($cart_data, $cart_item)
{
    $custom_items = array();

    if (!empty($cart_data))
        $custom_items = $cart_data;

    if (isset($cart_item["product_vars"])) {
        $arr_vars = $cart_item["product_vars"];

        foreach ($arr_vars as $key => $value) {
            if (($key == 'pa_extra_width') or ($key == 'pa_extra_height') or ($key == 'product_baseboard') or ($key == 'motorized')) {
                if ($key == 'pa_extra_width') {
                    $label = 'Extra Width';
                }
                if ($key == 'pa_extra_height') {
                    $label = 'Extra Width';
                }
                if ($key == 'product_baseboard') {
                    $label = 'With Baseboard';
                }
                if ($key == 'motorized') {
                    $label = 'Motorized Shades';
                }
                if ($value == 'on') {
                    $value = 'Yes';
                }
                if ($value == 'yes') {
                    $value = 'Yes';
                }


                $final_value = $value;
            } else {
                $temp = get_term_by('id', (int)$value, $key);
                $label =  wc_attribute_label($key);
                $final_value = $temp->name;
            }
            $custom_items[] = array(
                'name' => $label,
                'value' => $final_value,
                'display' => $final_value
            );
        }
    }

    return $custom_items;
}

add_filter('woocommerce_add_to_cart_fragments', 'diyflorida_add_to_cart_fragment');

function diyflorida_add_to_cart_fragment($fragments)
{

    global $woocommerce;

    wc_clear_notices();

    $fragments['.btn-cart'] = '<a href="' . wc_get_cart_url() . '" class="btn btn-md btn-link btn-cart"><img src="' . get_template_directory_uri() . '/images/cart.png" alt="Shopping Cart" class="img-fluid"/><span class="badge badge-warning">' . sprintf(_n('%s', '%s', $woocommerce->cart->cart_contents_count, 'diyflorida'), $woocommerce->cart->cart_contents_count) . '</span>' . __('YOUR SHOPPING CART', 'diyflorida') . '</a>';
    return $fragments;
}

//clear notices on cart update
function clear_notices_on_cart_update()
{
    wc_clear_notices();
};

add_filter('woocommerce_update_cart_action_cart_updated', 'clear_notices_on_cart_update', 10, 1);

add_action('wp_ajax_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');
add_action('wp_ajax_nopriv_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');

function woocommerce_ajax_add_to_cart()
{

    $product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
    $quantity = empty($_POST['quantity']) ? 1 : wc_stock_amount($_POST['quantity']);
    $variation_id = absint($_POST['variation_id']);
    $product_vars = $_POST['product_vars'];
    $product_price = $_POST['product_price'];
    $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);
    $product_status = get_post_status($product_id);

    if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id) && 'publish' === $product_status) {

        do_action('woocommerce_ajax_added_to_cart', $product_id);

        $cart = $woocommerce->cart->cart_contents;
        foreach ($cart as $key => $item) {
            // check if voucher meta already exists for an item, and if so, skip
            if ($item['product_vars']) {
                continue;
            } else {
                // make sure we only add voucher data to the variation just added and only if data isn't there already so we don't override previously added ones
                if ($item['variation_id'] === $variation_id && !isset($item['product_vars'])) {
                    $woocommerce->cart->cart_contents[$key]['product_vars'] = $product_vars;
                }
            }

            if ($item['product_price']) {
                continue;
            } else {
                // make sure we only add voucher data to the variation just added and only if data isn't there already so we don't override previously added ones
                if ($item['variation_id'] === $variation_id && !isset($item['product_price'])) {
                    $woocommerce->cart->cart_contents[$key]['product_price'] = $product_price;
                }
            }
        }

        if ('yes' === get_option('woocommerce_cart_redirect_after_add')) {
            wc_add_to_cart_message(array($product_id => $quantity), true);
        }

        WC_AJAX::get_refreshed_fragments();
    } else {

        $data = array(
            'error' => true,
            'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id)
        );

        echo wp_send_json($data);
    }

    wp_die();
}


add_action('woocommerce_proceed_to_checkout', 'cart_privacy_policy_checkbox', 99);
function cart_privacy_policy_checkbox()
{

    woocommerce_form_field(
        'privacy_policy',
        array(
            'type'          => 'checkbox',
            'class'         => array('form-row privacy'),
            'label_class'   => array('woocommerce-form__label woocommerce-form__label-for-checkbox checkbox'),
            'input_class'   => array('woocommerce-form__input woocommerce-form__input-checkbox input-checkbox'),
            'required'      => true,
            'label'         => esc_html__("I accept these are custom shades and cannot be returned nor refunded if measurements provided by me were incorrect", "woocommerce")
        ),
    );

    // jQuery code start below
    ?>
    <script type="text/javascript">
        jQuery(function($) {
            var a = '.checkout-button',
                b = '#privacy_policy',
                c = '<?php echo wc_get_checkout_url(); ?>',
                d = 'disabled';

            // Set disable button state
            $(a).addClass(d).prop('href', '#');

            // Woocommerce Ajax cart events
            $('body').on('updated_cart_totals removed_from_cart', function() {
                if (!($(a).hasClass(d) && $(b).prop('checked'))) {
                    $(a).addClass(d).prop('href', '#');
                }
            })

            // On button click event
            $('body').on('click', a, function(e) {
                if (!$(b).prop('checked')) {
                    // Disable "Proceed to checkout" button
                    e.preventDefault();
                    // disabling button state
                    if (!$(a).hasClass(d)) {
                        $(a).addClass(d).prop('href', '#');
                    }
                }
            });

            // On checkbox change event
            $(b).change(function() {
                if ($(this).prop('checked')) {
                    if ($(a).hasClass(d)) {
                        $(a).removeClass(d).prop('href', c);
                    }
                } else {
                    if (!$(a).hasClass(d)) {
                        $(a).addClass(d).prop('href', '#');
                    }
                }
            });
        });
    </script>
<?php
}
