<?php
/*
function diyflorida_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Clientes', 'Post Type General Name', 'diyflorida' ),
		'singular_name'         => _x( 'Cliente', 'Post Type Singular Name', 'diyflorida' ),
		'menu_name'             => __( 'Clientes', 'diyflorida' ),
		'name_admin_bar'        => __( 'Clientes', 'diyflorida' ),
		'archives'              => __( 'Archivo de Clientes', 'diyflorida' ),
		'attributes'            => __( 'Atributos de Cliente', 'diyflorida' ),
		'parent_item_colon'     => __( 'Cliente Padre:', 'diyflorida' ),
		'all_items'             => __( 'Todos los Clientes', 'diyflorida' ),
		'add_new_item'          => __( 'Agregar Nuevo Cliente', 'diyflorida' ),
		'add_new'               => __( 'Agregar Nuevo', 'diyflorida' ),
		'new_item'              => __( 'Nuevo Cliente', 'diyflorida' ),
		'edit_item'             => __( 'Editar Cliente', 'diyflorida' ),
		'update_item'           => __( 'Actualizar Cliente', 'diyflorida' ),
		'view_item'             => __( 'Ver Cliente', 'diyflorida' ),
		'view_items'            => __( 'Ver Clientes', 'diyflorida' ),
		'search_items'          => __( 'Buscar Cliente', 'diyflorida' ),
		'not_found'             => __( 'No hay resultados', 'diyflorida' ),
		'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'diyflorida' ),
		'featured_image'        => __( 'Imagen del Cliente', 'diyflorida' ),
		'set_featured_image'    => __( 'Colocar Imagen del Cliente', 'diyflorida' ),
		'remove_featured_image' => __( 'Remover Imagen del Cliente', 'diyflorida' ),
		'use_featured_image'    => __( 'Usar como Imagen del Cliente', 'diyflorida' ),
		'insert_into_item'      => __( 'Insertar en Cliente', 'diyflorida' ),
		'uploaded_to_this_item' => __( 'Cargado a este Cliente', 'diyflorida' ),
		'items_list'            => __( 'Listado de Clientes', 'diyflorida' ),
		'items_list_navigation' => __( 'Navegación del Listado de Cliente', 'diyflorida' ),
		'filter_items_list'     => __( 'Filtro del Listado de Clientes', 'diyflorida' ),
	);
	$args = array(
		'label'                 => __( 'Cliente', 'diyflorida' ),
		'description'           => __( 'Portafolio de Clientes', 'diyflorida' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'tipos-de-clientes' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 15,
		'menu_icon'             => 'dashicons-businessperson',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'clientes', $args );

}

add_action( 'init', 'diyflorida_custom_post_type', 0 );
*/
