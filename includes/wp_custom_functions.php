<?php

/* --------------------------------------------------------------
    ADD THEME SUPPORT
-------------------------------------------------------------- */
load_theme_textdomain('diyflorida', get_template_directory() . '/languages');
add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio'));
add_theme_support('post-thumbnails');
add_theme_support('automatic-feed-links');
add_theme_support('title-tag');
add_theme_support('menus');
add_theme_support('customize-selective-refresh-widgets');
add_theme_support(
    'custom-background',
    array(
        'default-image' => '',
        'default-color' => 'ffffff',
        'wp-head-callback' => '_custom_background_cb',
        'admin-head-callback' => '',
        'admin-preview-callback' => ''
    )
);
add_theme_support('custom-logo', array(
    'height'      => 250,
    'width'       => 250,
    'flex-width'  => true,
    'flex-height' => true,
));
add_theme_support('html5', array(
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
));

/* ADD SHORTCODE SUPPORT TO TEXT WIDGETS */
add_filter('widget_text', 'do_shortcode');

/* --------------------------------------------------------------
    SECURITY ISSUES
-------------------------------------------------------------- */
/* REMOVE WORDPRESS VERSION */
function diyflorida_remove_version()
{
    return '';
}
add_filter('the_generator', 'diyflorida_remove_version');

/* CHANGE WORDPRESS ERROR ON LOGIN */
function diyflorida_wordpress_errors()
{
    return __('Valores Incorrectos, intente de nuevo', 'diyflorida');
}
add_filter('login_errors', 'diyflorida_wordpress_errors');

/* DISABLE WORDPRESS RSS FEEDS */
function diyflorida_disable_feed()
{
    wp_die(__('No hay RSS Feeds disponibles', 'diyflorida'));
}

add_action('do_feed', 'diyflorida_disable_feed', 1);
add_action('do_feed_rdf', 'diyflorida_disable_feed', 1);
add_action('do_feed_rss', 'diyflorida_disable_feed', 1);
add_action('do_feed_rss2', 'diyflorida_disable_feed', 1);
add_action('do_feed_atom', 'diyflorida_disable_feed', 1);

/* --------------------------------------------------------------
    IMAGES RESPONSIVE ON ATTACHMENT IMAGES
-------------------------------------------------------------- */
function image_tag_class($class)
{
    $class .= ' img-fluid';
    return $class;
}
add_filter('get_image_tag_class', 'image_tag_class');

/* --------------------------------------------------------------
    ADD NAV ITEM TO MENU AND LINKS
-------------------------------------------------------------- */
function special_nav_class($classes, $item)
{
    $classes[] = 'nav-item';
    if (in_array('current-menu-item', $classes)) {
        $classes[] = 'active ';
    }
    return $classes;
}
add_filter('nav_menu_css_class', 'special_nav_class', 10, 2);

function add_menuclass($ulclass)
{
    return preg_replace('/<a /', '<a class="nav-link"', $ulclass);
}
add_filter('wp_nav_menu', 'add_menuclass');

/* --------------------------------------------------------------
    CUSTOM ADMIN LOGIN
-------------------------------------------------------------- */

function custom_admin_styles()
{
    $version_remove = NULL;
    wp_register_style('wp-admin-style', get_template_directory_uri() . '/css/custom-wordpress-admin-style.css', false, $version_remove, 'all');
    wp_enqueue_style('wp-admin-style');
}
add_action('login_head', 'custom_admin_styles');
add_action('admin_init', 'custom_admin_styles');

/* --------------------------------------------------------------
    CUSTOM ADMIN LOGO
-------------------------------------------------------------- */

function diyflorida_custom_logo()
{
    ob_start();
?>
    <style type="text/css">
        #wpadminbar #wp-admin-bar-wp-logo>.ab-item .ab-icon:before {
            background-image: url(<?php echo get_template_directory_uri('/');
                                    ?>/images/apple-touch-icon.png) !important;
            background-size: cover;
            background-position: 0 0;
            color: rgba(0, 0, 0, 0);
        }

        #wpadminbar #wp-admin-bar-wp-logo.hover>.ab-item .ab-icon {
            background-position: 0 0;
        }
    </style>
    <?php
    $content = ob_get_clean();
    echo $content;
}

add_action('wp_before_admin_bar_render', 'diyflorida_custom_logo');


/* --------------------------------------------------------------
    CUSTOM ADMIN FOOTER
-------------------------------------------------------------- */
function dashboard_footer()
{
    echo '<span id="footer-thankyou">';
    _e('Gracias por crear con ', 'diyflorida');
    echo '<a href="http://wordpress.org/" target="_blank">WordPress.</a> - ';
    _e('Tema desarrollado por ', 'diyflorida');
    echo '<a href="http://robertochoa.com.ve/?utm_source=footer_admin&utm_medium=link&utm_content=diyflorida" target="_blank">Robert Ochoa</a></span>';
}
add_filter('admin_footer_text', 'dashboard_footer');

/* --------------------------------------------------------------
    CUSTOM WIDGET CONTACT DATA
-------------------------------------------------------------- */
class custom_contact_data_widget extends WP_Widget
{

    function __construct()
    {
        parent::__construct(

            // Base ID of your widget
            'custom_contact_data_widget',

            // Widget name will appear in UI
            __('[DIY] Datos de Contacto', 'diyflorida'),

            // Widget description
            array('description' => __('Widget para mostrar con ícono los datos de contacto en un mediatype', 'diyflorida'),)
        );
    }

    // Creating widget front-end


    public function widget($args, $instance)
    {
        $title = apply_filters('widget_title', $instance['title']);
        $type = $instance['type'];
        $desc = $instance['description'];

        echo $args['before_widget'];
        ob_start();
    ?>
        <div class="custom-contact-data-container">
            <?php if (!empty($title)) { ?>
                <div class="custom-contact-data-title">
                    <?php echo $args['before_title'] . $title . $args['after_title']; ?>
                </div>
            <?php } ?>
            <?php if (!empty($desc)) { ?>
                <div class="media">
                    <?php switch ($type) {
                        case 'hours':
                            $image = 'icon_servicehours';
                            break;
                        case 'location':
                            $image = 'icon_address';
                            break;
                        case 'phone':
                            $image = 'icon_phone';
                            break;
                    } ?>
                    <img src="<?php echo get_template_directory_uri(); ?>/images/<?php echo $image; ?>.png" alt="<?php echo $type; ?>" class="img-fluid align-self-start" />
                    <div class="media-body">
                        <?php echo apply_filters('the_content', $desc); ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php
        $output = ob_get_contents();
        ob_end_clean();
        echo $output;
        echo $args['after_widget'];
    }

    // Widget Backend
    public function form($instance)
    {
        $title = (isset($instance['title'])) ? $instance['title'] : 'Titulo';
        $type = (isset($instance['type'])) ? $instance['type'] : 'hours';
        $desc = (isset($instance['description'])) ? $instance['description'] : 'Descripción';
    ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">
                <?php _e('Título:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('type'); ?>">
                <?php _e('Tipo de Ícono:'); ?></label>
            <select name="<?php echo $this->get_field_name('type'); ?>" id="<?php echo $this->get_field_id('type'); ?>">
                <option value="phone" <?php selected($type, 'phone'); ?>><?php _e('Teléfono', 'diyflorida'); ?></option>
                <option value="location" <?php selected($type, 'location'); ?>><?php _e('Ubicación', 'diyflorida'); ?></option>
                <option value="hours" <?php selected($type, 'hours'); ?>><?php _e('Horario', 'diyflorida'); ?></option>
            </select>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('description'); ?>">
                <?php _e('Texto descriptivo:'); ?></label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>" type="text"><?php echo esc_attr($desc); ?></textarea>
        </p>
    <?php
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['type'] = (!empty($new_instance['type'])) ? strip_tags($new_instance['type']) : '';
        $instance['description'] = (!empty($new_instance['description'])) ? strip_tags($new_instance['description']) : '';
        return $instance;
    }

    // Class wpb_widget ends here
}

/* --------------------------------------------------------------
    CUSTOM WIDGET CONTACT DATA
-------------------------------------------------------------- */
class custom_contact_form_widget extends WP_Widget
{

    function __construct()
    {
        parent::__construct(

            // Base ID of your widget
            'custom_contact_form_widget',

            // Widget name will appear in UI
            __('[DIY] Formulario de Contacto', 'diyflorida'),

            // Widget description
            array('description' => __('Widget para mostrar un formulario de contacto', 'diyflorida'),)
        );
    }

    // Creating widget front-end


    public function widget($args, $instance)
    {
        $title = apply_filters('widget_title', $instance['title']);
        $cta_text = $instance['cta_text'];

        echo $args['before_widget'];
        ob_start();
    ?>
        <div class="custom-widget-contact-form-container">
            <?php if (!empty($title)) { ?>
                <div class="widget-contact-form-title">
                    <?php echo $args['before_title'] . $title . $args['after_title']; ?>
                </div>
            <?php } ?>
            <form class="widget-contact-form-container" type="POST">
                <div class="widget-form-item">
                    <input id="fullname" name="fullname" class="form-control widget-form-control" type="text" placeholder="<?php _e('FULL NAME', 'diyflorida'); ?>">
                    <small id="fullnameError" class="error-widget d-none"></small>
                </div>
                <div class="widget-form-item">
                    <input id="email" name="email" class="form-control widget-form-control" type="email" placeholder="<?php _e('EMAIL ADDRESS', 'diyflorida'); ?>">
                    <small id="emailError" class="error-widget d-none"></small>
                </div>
                <div class="widget-form-item">
                    <textarea id="message" name="message" class="form-control widget-form-control" placeholder="<?php _e('MESSAGE', 'diyflorida'); ?>"></textarea>
                    <small id="messageError" class="error-widget d-none"></small>
                </div>
                <div class="widget-form-item">
                    <button type="submit" class="btn btn-md btn-widget" title="<?php echo $cta_text; ?>"><?php echo $cta_text; ?></button>
                    <div class="loader-css d-none"></div>
                </div>
            </form>
        </div>
    <?php
        $output = ob_get_contents();
        ob_end_clean();
        echo $output;
        echo $args['after_widget'];
    }

    // Widget Backend
    public function form($instance)
    {
        $title = (isset($instance['title'])) ? $instance['title'] : 'Titulo';
        $cta_text = (isset($instance['cta_text'])) ? $instance['cta_text'] : 'Llamado a Acción';
    ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">
                <?php _e('Título:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('cta_text'); ?>">
                <?php _e('Título:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('cta_text'); ?>" name="<?php echo $this->get_field_name('cta_text'); ?>" type="text" value="<?php echo esc_attr($cta_text); ?>" />
        </p>
<?php
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['cta_text'] = (!empty($new_instance['cta_text'])) ? strip_tags($new_instance['cta_text']) : '';
        return $instance;
    }
}


// Register and load the widget
function diy_load_widget()
{
    register_widget('custom_contact_data_widget');
    register_widget('custom_contact_form_widget');
}
add_action('widgets_init', 'diy_load_widget');
