<?php
/* --------------------------------------------------------------
    1.- HOME: SLIDER SECTION
-------------------------------------------------------------- */
$cmb_home_hero = new_cmb2_box( array(
    'id'            => $prefix . 'home_hero_metabox',
    'title'         => esc_html__( 'Home: Hero Principal', 'diyflorida' ),
    'object_types'  => array( 'page' ),
    'show_on'       => array( 'key' => 'page-template', 'value' => 'templates/page-prehome.php' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true,
    'cmb_styles'    => true,
    'closed'        => false
) );

$group_field_id = $cmb_home_hero->add_field( array(
    'id'            => $prefix . 'home_slider_group',
    'name'          => esc_html__( 'Grupos de Items', 'diyflorida' ),
    'description'   => __( 'Items dentro del Itemr', 'diyflorida' ),
    'type'          => 'group',
    'options'       => array(
        'group_title'       => __( 'Item {#}', 'diyflorida' ),
        'add_button'        => __( 'Agregar otro Item', 'diyflorida' ),
        'remove_button'     => __( 'Remover Item', 'diyflorida' ),
        'sortable'          => true,
        'closed'            => true,
        'remove_confirm'    => esc_html__( '¿Estas seguro de remover este Item?', 'diyflorida' )
    )
) );

$cmb_home_hero->add_group_field( $group_field_id, array(
    'id'        => 'bg_image',
    'name'      => esc_html__( 'Imagen de Fondo del Item', 'diyflorida' ),
    'desc'      => esc_html__( 'Cargar un fondo para este Item', 'diyflorida' ),
    'type'      => 'file',
    'options'   => array(
        'url'   => false
    ),
    'text'      => array(
        'add_upload_file_text' => esc_html__( 'Cargar fondo', 'diyflorida' ),
    ),
    'query_args' => array(
        'type'  => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'thumbnail'
) );

$cmb_home_hero->add_group_field( $group_field_id, array(
    'id'        => 'pretitle',
    'name'      => esc_html__( 'Pre-Título del Item', 'diyflorida' ),
    'desc'      => esc_html__( 'Ingrese el Título del Item', 'diyflorida' ),
    'type'      => 'text'
) );

$cmb_home_hero->add_group_field( $group_field_id, array(
    'id'        => 'title',
    'name'      => esc_html__( 'Titulo del Item', 'diyflorida' ),
    'desc'      => esc_html__( 'Ingrese la descripción del Item', 'diyflorida' ),
    'type'      => 'text'
) );

$cmb_home_hero->add_group_field( $group_field_id, array(
    'id'        => 'button_text',
    'name'      => esc_html__( 'Texto del Boton en Item', 'diyflorida' ),
    'desc'      => esc_html__( 'Ingrese el Texto del boton correspondiente', 'diyflorida' ),
    'type'      => 'text'
) );

$cmb_home_hero->add_group_field( $group_field_id, array(
    'id'        => 'button_link',
    'name'      => esc_html__( 'Link del Boton en Item', 'diyflorida' ),
    'desc'      => esc_html__( 'Ingrese el link de accion del boton correspondiente', 'diyflorida' ),
    'type'      => 'text'
) );

$cmb_home_hero->add_group_field( $group_field_id, array(
    'id'        => 'button_posttitle',
    'name'      => esc_html__( 'Pre-Título del Item', 'diyflorida' ),
    'desc'      => esc_html__( 'Ingrese el Título del Item', 'diyflorida' ),
    'type'      => 'text'
) );

/* --------------------------------------------------------------
    2.- HOME: TWO SIDED CONTENT
-------------------------------------------------------------- */
$cmb_home_twosided = new_cmb2_box( array(
    'id'            => $prefix . 'home_twosided_metabox',
    'title'         => esc_html__( 'Home: Sección de Imagen Lateral', 'diyflorida' ),
    'object_types'  => array( 'page' ),
    'show_on'       => array( 'key' => 'page-template', 'value' => 'templates/page-prehome.php' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true,
    'cmb_styles'    => true,
    'closed'        => false
) );

$cmb_home_twosided->add_field( array(
    'id'        => $prefix . 'twosided_image',
    'name'      => esc_html__( 'Imagen de Fondo del Item', 'diyflorida' ),
    'desc'      => esc_html__( 'Cargar un fondo para este Item', 'diyflorida' ),
    'type'      => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar fondo', 'diyflorida' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'thumbnail'
) );

$cmb_home_twosided->add_field( array(
    'id'        => $prefix . 'twosided_title',
    'name'      => esc_html__( 'Titulo de la Sección', 'diyflorida' ),
    'desc'      => esc_html__( 'Ingrese la descripción del Item', 'diyflorida' ),
    'type'      => 'text'
) );

$group_field_id = $cmb_home_twosided->add_field( array(
    'id'            => $prefix . 'twosided_group',
    'name'          => esc_html__( 'Grupos de Items', 'diyflorida' ),
    'description'   => __( 'Items dentro del Itemr', 'diyflorida' ),
    'type'          => 'group',
    'options'       => array(
        'group_title'       => __( 'Item {#}', 'diyflorida' ),
        'add_button'        => __( 'Agregar otro Item', 'diyflorida' ),
        'remove_button'     => __( 'Remover Item', 'diyflorida' ),
        'sortable'          => true,
        'closed'            => true,
        'remove_confirm'    => esc_html__( '¿Estas seguro de remover este Item?', 'diyflorida' )
    )
) );

$cmb_home_twosided->add_group_field( $group_field_id, array(
    'id'        => 'title',
    'name'      => esc_html__( 'Título del Item', 'diyflorida' ),
    'desc'      => esc_html__( 'Ingrese el Título del Item', 'diyflorida' ),
    'type'      => 'text'
) );

$cmb_home_twosided->add_group_field( $group_field_id, array(
    'id'        => 'desc',
    'name'      => esc_html__( 'Descripción del Item', 'diyflorida' ),
    'desc'      => esc_html__( 'Ingrese la descripción del Item', 'diyflorida' ),
    'type'      => 'wysiwyg',
    'options'   => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny'         => false
    )
) );

/* --------------------------------------------------------------
    2.- HOME: TWO SIDED CONTENT 2
-------------------------------------------------------------- */
$cmb_home_twosided2 = new_cmb2_box( array(
    'id'            => $prefix . 'home_twosided2_metabox',
    'title'         => esc_html__( 'Home: Sección de Imagen Lateral (Imagen a la Izquierda)', 'diyflorida' ),
    'object_types'  => array( 'page' ),
    'show_on'       => array( 'key' => 'page-template', 'value' => 'templates/page-prehome.php' ),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true,
    'cmb_styles'    => true,
    'closed'        => false
) );

$cmb_home_twosided2->add_field( array(
    'id'        => $prefix . 'twosided2_image',
    'name'      => esc_html__( 'Imagen de Fondo del Item', 'diyflorida' ),
    'desc'      => esc_html__( 'Cargar un fondo para este Item', 'diyflorida' ),
    'type'      => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar fondo', 'diyflorida' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'thumbnail'
) );

$cmb_home_twosided2->add_field( array(
    'id'        => $prefix . 'twosided2_title',
    'name'      => esc_html__( 'Titulo de la Sección', 'diyflorida' ),
    'desc'      => esc_html__( 'Ingrese la descripción del Item', 'diyflorida' ),
    'type'      => 'text'
) );

$group_field_id = $cmb_home_twosided2->add_field( array(
    'id'            => $prefix . 'twosided2_group',
    'name'          => esc_html__( 'Grupos de Items', 'diyflorida' ),
    'description'   => __( 'Items dentro del Itemr', 'diyflorida' ),
    'type'          => 'group',
    'options'       => array(
        'group_title'       => __( 'Item {#}', 'diyflorida' ),
        'add_button'        => __( 'Agregar otro Item', 'diyflorida' ),
        'remove_button'     => __( 'Remover Item', 'diyflorida' ),
        'sortable'          => true,
        'closed'            => true,
        'remove_confirm'    => esc_html__( '¿Estas seguro de remover este Item?', 'diyflorida' )
    )
) );

$cmb_home_twosided2->add_group_field( $group_field_id, array(
    'id'        => 'title',
    'name'      => esc_html__( 'Título del Item', 'diyflorida' ),
    'desc'      => esc_html__( 'Ingrese el Título del Item', 'diyflorida' ),
    'type'      => 'text'
) );

$cmb_home_twosided2->add_group_field( $group_field_id, array(
    'id'        => 'desc',
    'name'      => esc_html__( 'Descripción del Item', 'diyflorida' ),
    'desc'      => esc_html__( 'Ingrese la descripción del Item', 'diyflorida' ),
    'type'      => 'wysiwyg',
    'options'   => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny'         => false
    )
) );