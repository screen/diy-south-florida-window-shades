<?php
/* --------------------------------------------------------------
WP CUSTOMIZE SECTION - CUSTOM SETTINGS
-------------------------------------------------------------- */

add_action('customize_register', 'diyflorida_customize_register');

function diyflorida_customize_register($wp_customize)
{

    /* HEADER */
    $wp_customize->add_section('diy_header_settings', array(
        'title'    => __('Header', 'diyflorida'),
        'description' => __('Header elements options', 'diyflorida'),
        'priority' => 30
    ));

    $wp_customize->add_setting('diy_header_settings[button_text]', array(
        'default'           => '',
        'sanitize_callback' => '',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control('button_text', array(
        'type' => 'text',
        'label'    => __('Button Text', 'diyflorida'),
        'description' => __('Add the descriptive text for the header button', 'diyflorida'),
        'section'  => 'diy_header_settings',
        'settings' => 'diy_header_settings[button_text]'
    ));

    $wp_customize->add_setting('diy_header_settings[button_link]', array(
        'default'           => '',
        'sanitize_callback' => '',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control('button_link', array(
        'type' => 'text',
        'label'    => __('Button Link', 'diyflorida'),
        'description' => __('Add the URL Link for this header button', 'diyflorida'),
        'section'  => 'diy_header_settings',
        'settings' => 'diy_header_settings[button_link]'
    ));

    $wp_customize->add_setting('diy_header_settings[button_text2]', array(
        'default'           => '',
        'sanitize_callback' => '',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control('button_text2', array(
        'type' => 'text',
        'label'    => __('Button Text 2', 'diyflorida'),
        'description' => __('Add the descriptive text for the header button', 'diyflorida'),
        'section'  => 'diy_header_settings',
        'settings' => 'diy_header_settings[button_text2]'
    ));

    $wp_customize->add_setting('diy_header_settings[button_link2]', array(
        'default'           => '',
        'sanitize_callback' => '',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control('button_link2', array(
        'type' => 'text',
        'label'    => __('Button Link 2', 'diyflorida'),
        'description' => __('Add the URL link for this header button', 'diyflorida'),
        'section'  => 'diy_header_settings',
        'settings' => 'diy_header_settings[button_link2]'
    ));

    /* SOCIAL SETTINGS */
    $wp_customize->add_section('diy_social_settings', array(
        'title'    => __('Redes Sociales', 'diyflorida'),
        'description' => __('Agregue aqui las redes sociales de la página, serán usadas globalmente', 'diyflorida'),
        'priority' => 175,
    ));

    $wp_customize->add_setting('diy_social_settings[facebook]', array(
        'default'           => '',
        'sanitize_callback' => 'diyflorida_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control('facebook', array(
        'type' => 'url',
        'section' => 'diy_social_settings',
        'settings' => 'diy_social_settings[facebook]',
        'label' => __('Facebook', 'diyflorida'),
    ));

    $wp_customize->add_setting('diy_social_settings[twitter]', array(
        'default'           => '',
        'sanitize_callback' => 'diyflorida_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control('twitter', array(
        'type' => 'url',
        'section' => 'diy_social_settings',
        'settings' => 'diy_social_settings[twitter]',
        'label' => __('Twitter', 'diyflorida'),
    ));

    $wp_customize->add_setting('diy_social_settings[instagram]', array(
        'default'           => '',
        'sanitize_callback' => 'diyflorida_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('instagram', array(
        'type' => 'url',
        'section' => 'diy_social_settings',
        'settings' => 'diy_social_settings[instagram]',
        'label' => __('Instagram', 'diyflorida'),
    ));

    $wp_customize->add_setting('diy_social_settings[linkedin]', array(
        'default'           => '',
        'sanitize_callback' => 'diyflorida_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control('linkedin', array(
        'type' => 'url',
        'section' => 'diy_social_settings',
        'settings' => 'diy_social_settings[linkedin]',
        'label' => __('LinkedIn', 'diyflorida'),
    ));

    $wp_customize->add_setting('diy_social_settings[youtube]', array(
        'default'           => '',
        'sanitize_callback' => 'diyflorida_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('youtube', array(
        'type' => 'url',
        'section' => 'diy_social_settings',
        'settings' => 'diy_social_settings[youtube]',
        'label' => __('YouTube', 'diyflorida'),
    ));

    /* COOKIES SETTINGS */
    $wp_customize->add_section('diy_cookie_settings', array(
        'title'    => __('Cookies', 'diyflorida'),
        'description' => __('Opciones de Cookies', 'diyflorida'),
        'priority' => 176,
    ));

    $wp_customize->add_setting('diy_cookie_settings[cookie_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control('cookie_text', array(
        'type' => 'textarea',
        'label'    => __('Cookie consent', 'diyflorida'),
        'description' => __('Texto del Cookie consent.'),
        'section'  => 'diy_cookie_settings',
        'settings' => 'diy_cookie_settings[cookie_text]'
    ));

    $wp_customize->add_setting('diy_cookie_settings[cookie_link]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('cookie_link', array(
        'type'     => 'dropdown-pages',
        'section' => 'diy_cookie_settings',
        'settings' => 'diy_cookie_settings[cookie_link]',
        'label' => __('Link de Cookies', 'diyflorida'),
    ));
}

function diyflorida_sanitize_url($url)
{
    return esc_url_raw($url);
}


function custom_admin_scripts_css()
{
}

/* --------------------------------------------------------------
CUSTOM AREA FOR OPTIONS DATA - diy
-------------------------------------------------------------- */

/* CUSTOM MENU PAGE AND FUNCTIONS IN ADMIN */
function register_diy_settings()
{
    //register our settings
    register_setting('diy-settings-group', 'prices_table_1');
    register_setting('diy-settings-group', 'prices_table_2');
    register_setting('diy-settings-group', 'prices_table_3');
    register_setting('diy-settings-group', 'prices_sideguide');
}

function my_admin_menu()
{
    add_submenu_page('woocommerce', 'Precios Adicionales', 'Precios Adicionales', 'edit_posts', 'diy_custom_options', 'my_custom_menu_page', 120);
    add_action('admin_init', 'register_diy_settings');
}

add_action('admin_menu', 'my_admin_menu');

/* CUSTOM CSS FOR THIS SECTION */
function load_custom_wp_admin_style($hook)
{
    if ($hook != 'woocommerce_page_diy_custom_options') {
        return;
    }
    wp_enqueue_script('handsometable_js', 'https://cdn.jsdelivr.net/npm/handsontable/dist/handsontable.full.min.js', array('jquery'), null, true);
    wp_enqueue_style('handsometable_css', 'https://cdn.jsdelivr.net/npm/handsontable/dist/handsontable.full.min.css', array(), null, 'all');
    wp_enqueue_style('custom_wp_admin_css', get_template_directory_uri() . '/css/custom-wordpress-admin-style.css');
    wp_enqueue_script('custom_wp_admin_js',  get_template_directory_uri() . '/js/custom-wordpress-admin-script.js', array('jquery', 'handsometable_js'), null, true);
    wp_localize_script('custom_wp_admin_js', 'custom_admin_url', array(
        'custom_table_price_1' => get_option('prices_table_1'),
        'custom_table_price_2' => get_option('prices_table_2'),
        'custom_table_price_3' => get_option('prices_table_3'),
        'custom_sideguide_price' => get_option('prices_sideguide'),
        'custom_window_width' => ajax_get_attributes_prices_width_handler(),
        'custom_window_height' => ajax_get_attributes_prices_height_handler()
    ));
}

add_action('admin_enqueue_scripts', 'load_custom_wp_admin_style');

/* CUSTOM MENU PAGE CONTENT */
function my_custom_menu_page()
{ ?>

    <div class="diy_custom_options-header">
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="<?php echo get_bloginfo('name'); ?>" class="logo-header" />
        <h1><?php echo get_admin_page_title(); ?></h1>
    </div>
    <div class="diy_custom_options-content">
        <form method="post" action="options.php" class="form-diy-container">
            <?php settings_fields('diy-settings-group'); ?>
            <?php do_settings_sections('diy-settings-group'); ?>
            <div class="special-table-container">
                <h2>Tabla Precios 3% - 5% - 10%</h2>
                <div id="specialPrice1" class="special-price-table">

                </div>
                <button id="saveTable1" class="special-table-btn">Guardar Cambios</button>
                <div id="loader1" class="loader-css loader-hidden"></div>
                <div id="responseTable1"></div>
            </div>
            <div class="special-table-container">
                <h2>Tabla Precios 1% - Blackout</h2>
                <div id="specialPrice2" class="special-price-table">

                </div>
                <button id="saveTable2" class="special-table-btn">Guardar Cambios</button>
                <div id="loader2" class="loader-css loader-hidden"></div>
                <div id="responseTable2"></div>
            </div>
            <div class="special-table-container">
                <h2>Tabla Precios Fascia</h2>
                <div id="specialPrice3" class="special-price-table">

                </div>
                <button id="saveTable3" class="special-table-btn">Guardar Cambios</button>
                <div id="loader3" class="loader-css loader-hidden"></div>
                <div id="responseTable3"></div>

                <h2>Precios SideGuide por Pies</h2>
                <input id="sideguideInput" type="text" name="pricesideguide" class="form-control" value="<?php echo esc_attr( get_option('prices_sideguide') ); ?>">
                <button id="saveTable4" class="special-table-btn">Guardar Cambios</button>
                <div id="loader4" class="loader-css loader-hidden"></div>
                <div id="responseTable4"></div>
            </div>
        </form>
    </div>
<?php }
add_action('admin_enqueue_scripts', 'custom_admin_scripts_css', 99);
