![alt tag](images/repo-logo.jpg)

# South Florida Window Shades: DIY - Wordpress Custom Theme #

* Version: 1.0.0
* Design: [Robert Ochoa](http://www.robertochoa.com.ve/?utm_source=github_link&utm_medium=link&utm_content=diyflorida)
* Development: [Robert Ochoa](http://www.robertochoa.com.ve/?utm_source=github_link&utm_medium=link&utm_content=diyflorida)

Tema diseñado por [Robert Ochoa](http://www.robertochoa.com.ve/?utm_source=github_link&utm_medium=link&utm_content=diyflorida) para South Florida Window Shades.
Este tema custom fue construido en su totalidad, pasando por su etapa de Wireframing, rearmado, version anterior e implementación en hosting externo.

### Componentes Principales ###

* Twitter Bootstrap 4.5.3

### Funciones Incluídas ###

* Custom Post Type.
* Custom Taxonomies.
* Bootstrap Ready: Wordpress Menu Structure.
* Custom Metabox.

### Plugins Requeridos ###

* CMB2
* Jetpack by Wordpress

### Instrucciones de Instalación ###

1. Instalar los plugins requeridos.

2. Activar los plugins requeridos.

3. Instalar el tema via FTP o por el instalador de themes de Wordpress via zip

### Contacto ###

Soporte Oficial para este tema:

Repo Owner: [Robert Ochoa](http://www.robertochoa.com.ve/?utm_source=github_link&utm_medium=link&utm_content=diyflorida)

Main Developer: [Robert Ochoa](http://www.robertochoa.com.ve/?utm_source=github_link&utm_medium=link&utm_content=diyflorida)
